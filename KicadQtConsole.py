import pcbnew
# http://docs.kicad-pcb.org/doxygen/md_Documentation_development_pcbnew-plugins.html
# https://github.com/jupyter/qtconsole/blob/master/examples/inprocess_qtconsole.py
# https://python.hotexamples.com/de/examples/qtconsole.inprocess/QtInProcessKernelManager/-/python-qtinprocesskernelmanager-class-examples.html


from qtconsole.qt import QtGui
from qtconsole.rich_jupyter_widget import RichJupyterWidget
from qtconsole.inprocess import QtInProcessKernelManager
from ipykernel.inprocess.ipkernel import InProcessKernel



class MyQtInProcessKernelManager(QtInProcessKernelManager):
    def start_kernel(self, namespace):
        self.kernel = InProcessKernel(parent=self, session=self.session, user_ns = namespace)

def show(namespace):
    global ipython_widget  # Prevent from being garbage collected

    # Create an in-process kernel
    kernel_manager = MyQtInProcessKernelManager(user_ns=namespace)
    kernel_manager.start_kernel(namespace) #show_banner=False)
    kernel = kernel_manager.kernel
    kernel.gui = 'qt4'
    kernel.shell.banner1 = "Test Shell"

    kernel_client = kernel_manager.client()
    kernel_client.start_channels()

    ipython_widget = RichJupyterWidget()
    ipython_widget.kernel_manager = kernel_manager
    ipython_widget.kernel_client = kernel_client
    ipython_widget.show()

class PluginQtConsole(pcbnew.ActionPlugin):
    def defaults(self):
        self.name = "QtConsole"
        self.category = "Shell"
        self.description = "Lauches the jupyter Qt console"
        self.show_toolbar_button = True # Optional, defaults to False
#        self.icon_file_name = os.path.join(os.path.dirname(__file__), 'simple_plugin.png') # Optional, defaults to ""

        import imp
        module = imp.new_module('__main__')
        import builtins
        module.__dict__['__builtins__'] = builtins
        module.__dict__['pcbnew'] = pcbnew
        self.namespace = module.__dict__.copy()

    def Run(self):
        app = QtGui.QApplication([])
        show(self.namespace)
        app.exec_()

PluginQtConsole().register()

