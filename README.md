# KicadQtConsole.py

Launches a jupyter qt console in pcbnew

On Arch:
```
pacman -S python-qtconsole
cp KicadQtConsole.py ~/.kicad_plugins/.
```
